////  SwiftUI2_AfricaApp.swift
//  SwiftUI2_Africa
//
//  Created on 27/01/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_AfricaApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
