////  ContentView.swift
//  SwiftUI2_Africa
//
//  Created on 27/01/2021.
//  
//

import SwiftUI

struct ContentView: View {
    
    let animals: [Animal] = Bundle.main.decode("animals.json")
    
    var body: some View {
        
        NavigationView {
            List {
                CoverImageView()
                    .listRowInsets(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
                    .frame(height: 300)

                ForEach(animals) { item in
                    AnimalListItemView(animal: item)
                }
                
            } //: List
            .navigationBarTitle("Africa", displayMode: .large)
        } //: NavigationView
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
