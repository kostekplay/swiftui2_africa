////  CoverImageView.swift
//  SwiftUI2_Africa
//
//  Created on 27/01/2021.
//  
//

import SwiftUI

struct CoverImageView: View {
    
    var body: some View {
        
        let coverImages: [CoverImage] = Bundle.main.decode("covers.json")
        
        TabView {
            ForEach(coverImages) { item in
                Image(item.name)
                    .resizable()
                    .scaledToFill()
            } //: ForEach
        } //: TabView
        .tabViewStyle(PageTabViewStyle())
        
    }
}

struct CoverImageView_Previews: PreviewProvider {
    static var previews: some View {
        CoverImageView()
            .previewLayout(.fixed(width: 400, height: 300))
            .padding()
    }
}
