////  CoverImageModel.swift
//  SwiftUI2_Africa
//
//  Created on 27/01/2021.
//  
//

import SwiftUI

struct CoverImage: Codable, Identifiable {
    let id: Int
    let name: String
}


